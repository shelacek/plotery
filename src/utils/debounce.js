export function debounce(callback, delay) {
	let timerId;
	return function (...args) {
		clearTimeout(timerId);
		timerId = setTimeout(() => callback.apply(this, args), delay);
	};
}
