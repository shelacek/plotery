export default {
	extends: 'stylelint-config-standard-scss',
	rules: {
		'alpha-value-notation': ['percentage', {
			exceptProperties: [
				'opacity',
				'fill-opacity',
				'flood-opacity',
				'stop-opacity',
				'stroke-opacity'
			]
		}],
		'at-rule-empty-line-before': ['always', {
			except: ['blockless-after-blockless', 'first-nested'],
			ignore: ['after-comment'],
			ignoreAtRules: ['else']
		}],
		'at-rule-no-vendor-prefix': true,
		'color-function-notation': 'modern',
		'color-hex-length': 'short',
		'comment-empty-line-before': ['always', {
			except: ['first-nested'],
			ignore: ['stylelint-commands']
		}],
		'comment-whitespace-inside': 'always',
		'custom-property-empty-line-before': ['always', {
			except: ['after-custom-property', 'first-nested'],
			ignore: ['after-comment', 'inside-single-line-block']
		}],
		'declaration-block-no-redundant-longhand-properties': true,
		'declaration-block-single-line-max-declarations': 1,
		'declaration-empty-line-before': ['always', {
			except: ['after-declaration', 'first-nested'],
			ignore: ['after-comment', 'inside-single-line-block']
		}],
		'font-family-name-quotes': 'always-where-recommended',
		'function-name-case': 'lower',
		'function-url-quotes': 'always',
		'hue-degree-notation': 'angle',
		'import-notation': 'string',
		'keyframe-selector-notation': 'percentage-unless-within-keyword-only-block',
		'length-zero-no-unit': [true, {
			ignore: ['custom-properties'],
		}],
		'lightness-notation': 'percentage',
		'media-feature-name-no-vendor-prefix': true,
		'media-feature-range-notation': 'context',
		'number-max-precision': 4,
		'property-no-vendor-prefix': true,
		'rule-empty-line-before': ['always-multi-line', {
			except: ['first-nested'],
			ignore: ['after-comment']
		}],
		'selector-attribute-quotes': 'always',
		'selector-no-vendor-prefix': true,
		'selector-not-notation': 'complex',
		'selector-pseudo-element-colon-notation': 'double',
		'selector-type-case': 'lower',
		'shorthand-property-no-redundant-values': true,
		'value-keyword-case': ['lower', {
			camelCaseSvgKeywords: true
		}],
		'value-no-vendor-prefix': [true, {
			ignoreValues: ['box', 'inline-box']
		}],
		'scss/at-else-closing-brace-newline-after': 'always-last-in-chain',
		'scss/at-else-closing-brace-space-after': 'always-intermediate',
		'scss/at-else-empty-line-before': 'never',
		'scss/at-else-if-parentheses-space-before': 'always',
		'scss/at-function-parentheses-space-before': 'never',
		'scss/at-if-closing-brace-newline-after': 'always-last-in-chain',
		'scss/at-if-closing-brace-space-after': 'always-intermediate',
		'scss/at-mixin-argumentless-call-parentheses': 'always',
		'scss/at-mixin-parentheses-space-before': 'never',
		'scss/at-rule-conditional-no-parentheses': true,
		'scss/dollar-variable-colon-space-after': 'always-single-line',
		'scss/dollar-variable-colon-space-before': 'never',
		'scss/dollar-variable-empty-line-before': ['always', {
			except: ['after-dollar-variable', 'first-nested'],
			ignore: ['after-comment', 'inside-single-line-block'],
		}],
		'scss/double-slash-comment-empty-line-before': ['always', {
			except: ['first-nested'],
			ignore: ['between-comments', 'stylelint-commands'],
		}],
		'scss/double-slash-comment-whitespace-inside': 'always'
	}
};
